# Packaging GNUnet for RPM #

This is the [Fedora package](https://git.gnunet.org/gnunet-rpm.git/tree/rpmbuild/SPECS/gnunet-fedora.spec).
If you want to port this to other RPM-based distros please use a branch.

## Resources ##

- [Install Tutorials](https://gnunet.org/en/install.html)
- [Install Section in Handbook](https://docs.gnunet.org/handbook/gnunet.html#Installing-GNUnet)

- [Alpine Package Patch](https://lists.alpinelinux.org/~alpine/aports/%3C20191006183040.6456-1-xrs%40mail36.net%3E#%3C20191006183040.6456-1-xrs@mail36.net%3E+testing/gnunet/APKBUILD)
- [Alpine Package MR in their Gitlab](https://gitlab.alpinelinux.org/alpine/aports/merge_requests/815/diffs)

- [GNUnet in Debian](https://tracker.debian.org/pkg/gnunet) ! Currently a bit old, but still useful to look at !
